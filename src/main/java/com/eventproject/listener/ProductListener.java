package com.eventproject.listener;

import com.eventproject.dto.ProductDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


/**
 * @author daniel.kouame
 * @created 17/05/2023
 * @project SpringBootEventListener-master
 */
@Component
public class ProductListener {

  @Async
  @EventListener
  void sendMsgEvent( ProductDto  product) {
    List<ProductDto> productDtoList = new ArrayList<>();
    productDtoList.add(product);
    productDtoList.stream().forEach(o-> System.out.println(o.getProductName()));
  }

  @Async
  @EventListener
  void sendMsgEvent(String message) {
    System.out.println("==EmailListener 2 ==="+message);
  }
}


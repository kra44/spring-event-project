package com.eventproject.controller;

import com.eventproject.dto.ProductDto;
import com.eventproject.publisher.ProductBlisher;
import com.eventproject.service.ProductService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    @Autowired
     ProductService productService;
    @Autowired
    ProductBlisher productBlisher;
    @GetMapping("/product")
    public List<ProductDto> getAllProduct(){
      Map<Integer, ProductDto> map = new HashMap<>();
       List<ProductDto> productList = productService.getProduct().stream()
           .map(ProductDto::fromEntity).collect(Collectors.toList());
       productList.stream().forEach(o->productBlisher.publisherProduct(o));
      ;
       return productList;
    }
}

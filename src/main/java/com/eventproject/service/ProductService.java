package com.eventproject.service;

import com.eventproject.entity.Product;
import com.eventproject.repository.ProducRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author daniel.kouame
 * @created 17/05/2023
 * @project SpringBootEventListener-master
 */
@Service
public class ProductService {
  @Autowired
  private ProducRepository producRepository;

  public List<Product> getProduct(){
    return producRepository.findAll();
  }

}


package com.eventproject.dto;

import com.eventproject.entity.Product;

/**
 * @author daniel.kouame
 * @created 17/05/2023
 * @project SpringBootEventListener-master
 */

public class ProductDto {
  private Integer id;
  private String productName;
  private Double productPrice;

  public ProductDto(Integer id, String productName, Double productPrice) {
    this.id = id;
    this.productName = productName;
    this.productPrice = productPrice;
  }

  public ProductDto() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public Double getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(Double productPrice) {
    this.productPrice = productPrice;
  }

  public static ProductDto fromEntity(Product product){
       ProductDto productDto = new ProductDto();
       productDto.setId(product.getId());
       productDto.setProductName(product.getProductName());
       productDto.setProductPrice(product.getProductPrice());
       return productDto;
  }
}


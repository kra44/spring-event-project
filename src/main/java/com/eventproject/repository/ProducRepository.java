package com.eventproject.repository;

import com.eventproject.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author daniel.kouame
 * @created 17/05/2023
 * @project SpringBootEventListener-master
 */
@Repository
public interface ProducRepository  extends JpaRepository<Product, Integer> {

}


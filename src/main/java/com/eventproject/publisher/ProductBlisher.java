package com.eventproject.publisher;

import com.eventproject.dto.ProductDto;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * @author daniel.kouame
 * @created 18/05/2023
 * @project SpringBootEventListener-master
 */

@Component
public class ProductBlisher {
  private final ApplicationEventPublisher eventPublisher;

  ProductBlisher(ApplicationEventPublisher publisher) {
    this.eventPublisher = publisher;
  }
  public void publisherProduct(ProductDto productDto){
    eventPublisher.publishEvent(productDto);
  }

}

